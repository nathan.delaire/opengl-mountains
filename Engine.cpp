//
// Created by natha on 11/07/2021.
//

#include "Engine.hpp"

//=================== INIT ==================//

Engine::Engine(int wWidth, int wHeight, const char *title) :
        W_WIDTH(wWidth), W_HEIGHT(wHeight), title(title){

    window = nullptr;

    HM_width = 600;
    HM_height = 600;
    HM_vertHeight = 2;
    HM_vertWidth = 2;
    HM_amplitude = 700 / 0.35;
    HM_frequency = 0.5f;
    HM_power = 0.125f;

    sun_x_orientation = 20000.f;

    initGlfw();
    initWindow();
    initImGUI();
    initGlew();
    initOpenGLOptions();

    initHeightMap();
    initLights();
    initShadowMap();

    initShaders();
    initTextures();
    initModel();

    initCamera();
    initMatrices();

    initSkyBox();
    initTextureBindings();
    initSun();

    initUniforms();

}

void Engine::initGlfw() {
    if(glfwInit() == GLFW_FALSE) {
        std::cout << "ERROR::GLFW_INIT_FAILED" << std::endl;
        glfwTerminate();
    }
}

void Engine::framebuffer_resize_callback(GLFWwindow *window, int fbW, int fbH) {
    glViewport(0, 0, fbW, fbH);
}

void Engine::initWindow() {
    const int GLMajor = 4;
    const int GLMinor = 4;

    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, GLMajor);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, GLMinor);

    glfwWindowHint(GLFW_RESIZABLE, true);

    window = glfwCreateWindow(W_WIDTH, W_HEIGHT, title, NULL, NULL);

    if(!window)
    {
        std::cout << "ERROR::INIT_WINDOW_FAILED" << std::endl;
        glfwTerminate();
    }

    fbwidth = W_WIDTH;
    fbheight = W_HEIGHT;

    glfwGetFramebufferSize(window, &fbwidth, &fbheight);
    glfwSetFramebufferSizeCallback(window, framebuffer_resize_callback);

    glfwMakeContextCurrent(window);
}

void Engine::initImGUI() {
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO();

    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init("#version 440");
    ImGui::StyleColorsLight();
}


void Engine::initGlew() {

    glewExperimental = GL_TRUE;
    if(glewInit() != GLEW_OK)
    {
        std::cout << "ERROR::GLEW_INIT_FAILED" << std::endl;
        glfwTerminate();
    }
}

void Engine::initOpenGLOptions() {
    //OpenGL Options
    glEnable(GL_DEPTH_TEST);

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    //Show cursor
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
}

void Engine::initHeightMap() {
    heightMapImage = createHeightMap(HM_width, HM_height, HM_vertWidth, HM_vertHeight,
                                     HM_frequency, HM_power);
}


void Engine::initShaders() {
    shader = new Shader("../shaders/vertex_core.glsl",
                        nullptr,
                        "../shaders/fragment_core.glsl");
}

void Engine::initTextures() {
    heightmap = new Texture("../img/heightmap.bmp");
    shadowmap = new Texture("../img/shadowmap.bmp");
    groud_grass = new Texture("../img/grass.png");
    ground_rock1 = new Texture("../img/rock.png");
    ground_rock2 = new Texture("../img/rock2.png");
    ground_snow = new Texture("../img/snow.png");
    mountain_logo = new Texture("../img/logo.png");
}

void Engine::initLights() {
    sunlight = new Light{glm::vec3(sun_x_orientation, 40000.f, 300),
                         glm::vec3(0.66f, -0.5f, 0.55f),
                         glm::vec3(1.f)};
}

void Engine::initModel() {
    const float SIZE = 10000;
    const int VERTEX_COUNT = 2000; // Default 2000

    // Map of [VERTEX_COUNT, VERTEX_COUNT]
    auto* vertices = new float[VERTEX_COUNT * VERTEX_COUNT * 3];
    auto* textureCoords = new float[VERTEX_COUNT * VERTEX_COUNT * 2];
    auto *indices = new unsigned int [6 * (VERTEX_COUNT - 1) * (VERTEX_COUNT - 1)];

    int vertexPointer = 0;
    for(float i = 0; i < VERTEX_COUNT; i++) { // z
        for(float j = 0; j < VERTEX_COUNT; j++) { // x
            //vertices[vertexPointer * 3] = j * 2;
            vertices[vertexPointer * 3] = (float)j / ((float)VERTEX_COUNT - 1) * SIZE;
            vertices[vertexPointer * 3 + 1] = 0;
            vertices[vertexPointer * 3 + 2] = (float)i / ((float)VERTEX_COUNT - 1) * SIZE;
            //vertices[vertexPointer * 3 + 2] = i * 2;

            textureCoords[vertexPointer * 2] = (float)j / ((float)VERTEX_COUNT - 1);
            textureCoords[vertexPointer * 2 + 1] = (float)i / ((float)VERTEX_COUNT - 1);
            vertexPointer++;
        }
    }

    int pointer = 0;
    for(int gz = 0; gz < VERTEX_COUNT - 1; gz++) {
        for(int gx = 0; gx < VERTEX_COUNT - 1; gx++) {
            int topLeft = (gz * VERTEX_COUNT) + gx;
            int topRight = topLeft + 1;
            int bottomLeft = ((gz + 1) * VERTEX_COUNT) + gx;
            int bottomRight = bottomLeft + 1;
            indices[pointer++] = topLeft;
            indices[pointer++] = bottomLeft;
            indices[pointer++] = topRight;
            indices[pointer++] = topRight;
            indices[pointer++] = bottomLeft;
            indices[pointer++] = bottomRight;
        }
    }

    ground = new Model(vertices, VERTEX_COUNT * VERTEX_COUNT * 3,
                       textureCoords, VERTEX_COUNT * VERTEX_COUNT * 2,
                       indices, 6 * (VERTEX_COUNT - 1) * (VERTEX_COUNT - 1));
}

void Engine::initCamera() {
    camera = Camera(glm::vec3(5054.2f, 233.889f,2588.51f),
                    glm::vec3(0.f, -1.f, 0.f),
                    glm::vec3(0.f, 1.f, 0.f));
}


void Engine::initMatrices() {
    ProjectionMatrix = glm::perspective(glm::radians(camera.fov), static_cast<float>(fbwidth) / fbheight,
                                        0.1f, 100000.f);
    ViewMatrix = camera.getViewMatrix();
    ModelMatrix = glm::translate(glm::mat4(), glm::vec3(0.f));
}

void Engine::initTextureBindings() {
    shader->use();
    glActiveTexture(GL_TEXTURE0);
    groud_grass->bind();
    glActiveTexture(GL_TEXTURE1);
    ground_rock1->bind();
    glActiveTexture(GL_TEXTURE2);
    ground_rock2->bind();
    glActiveTexture(GL_TEXTURE3);
    ground_snow->bind();
    glActiveTexture(GL_TEXTURE4);
    heightmap->bind();
    glActiveTexture(GL_TEXTURE5);
    shadowmap->bind();
    shader->unuse();
}


void Engine::initUniforms() {
    shader->use();
    shader->setTexture("tex_grass", 0);
    shader->setTexture("tex_rock1", 1);
    shader->setTexture("tex_rock2", 2);
    shader->setTexture("tex_snow", 3);
    shader->setTexture("heightMap", 4);
    shader->setTexture("shadowMap", 5);
    shader->setTexture("skybox", 6);
    shader->setFloat("AMPLITUDE", HM_amplitude);
    shader->unuse();
}

int _round(float n)
{
    if (n-((int)n) >= 0.5)
        return (int)n+1;
    else
        return (int)n;
}

void Engine::initShadowMap() {
    glm::vec3 currentPos;
    glm::vec3 lightDir;
    int LerpX, LerpZ;

    utils::Image ShadowMap;
    ShadowMap.SetSize(HM_width, HM_height);

    for(auto z = 0; z < HM_width; z++){
        for(auto x = 0; x < HM_height; x++){
            currentPos = glm::vec3((float)x, heightMapImage.GetValue(x, z).red, (float)z);
            lightDir = sunlight->position - currentPos;
            lightDir = glm::normalize(lightDir);
            ShadowMap.SetValue(x, z, utils::Color(255, 255, 255, 255));

            while ( currentPos.x >= 0 &&
                    currentPos.x < HM_width &&
                    currentPos.z >= 0 &&
                    currentPos.z < HM_height &&
                    currentPos != sunlight->position && currentPos.y < 255 )
            {
                currentPos += lightDir;
                LerpX = _round(currentPos.x);
                LerpZ = _round(currentPos.z);

                //Check if hit
                if(currentPos.y <= heightMapImage.GetValue(LerpX, LerpZ).red){
                    ShadowMap.SetValue(x, z, utils::Color(100, 100, 100, 255));
                    break;
                }
            }

        }
    }
    utils::WriterBMP writer;
    writer.SetSourceImage(ShadowMap);
    writer.SetDestFilename("../img/shadowmap.bmp");
    writer.WriteDestFile();
}

void Engine::initSkyBox() {
    skybox = new Skybox(ProjectionMatrix, ViewMatrix);
}

void Engine::initSun() {
    sun = new Sun(sunlight->position, ProjectionMatrix, ViewMatrix);
}






//============ OTHERS ======================//

Engine::~Engine() {
    glfwDestroyWindow(window);
    glfwTerminate();
}


GLFWwindow *Engine::getWindow() const {
    return window;
}

//=================== UPDATE ==================//

void Engine::updateDeltaTime() {
    //Get current time
    curTime = static_cast<float>(glfwGetTime());
    //Check update diff
    dt = curTime - lastTime;
    //Update last time
    lastTime = curTime;
}


void Engine::updateKeyboardInput()
{
    //Quit
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);

    //Camera
    if(glfwGetKey(window , GLFW_KEY_W) == GLFW_PRESS)
        camera.updateKeyboardInput(dt, FORWARD);
    if(glfwGetKey(window , GLFW_KEY_A) == GLFW_PRESS)
        camera.updateKeyboardInput(dt, LEFT);
    if(glfwGetKey(window , GLFW_KEY_D) == GLFW_PRESS)
        camera.updateKeyboardInput(dt, RIGHT);
    if(glfwGetKey(window , GLFW_KEY_S) == GLFW_PRESS)
        camera.updateKeyboardInput(dt, BACKWARD);
    if(glfwGetKey(window , GLFW_KEY_SPACE) == GLFW_PRESS)
        camera.updateKeyboardInput(dt, UP);
    if(glfwGetKey(window , GLFW_KEY_C) == GLFW_PRESS)
        camera.updateKeyboardInput(dt, DOWN);

    //Sprint
    if(glfwGetKey(window , GLFW_KEY_Q) == GLFW_PRESS)
        camera.setMovementSpeed(1500.f);
    else
        camera.setMovementSpeed(300.f);


    //Pause management
    if(glfwGetKey(window, GLFW_KEY_F1) == GLFW_PRESS) {
        if(!is_Pause){
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
            is_Pause = true;
        }
    }
    if(glfwGetKey(window, GLFW_KEY_F2) == GLFW_PRESS){
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        is_Pause = false;
        glfwSetCursorPos(window, mouseX, mouseY);
    }

}

void Engine::updateMouseInput() {
    glfwGetCursorPos(window, &mouseX, &mouseY);

    if(firstMouse)
    {
        lastMouseX = mouseX;
        lastMouseY = mouseY;
        firstMouse = false;
    }

    mouseOffsetX = mouseX - lastMouseX;
    mouseOffsetY = mouseY - lastMouseY;

    lastMouseX = mouseX;
    lastMouseY = mouseY;
}

void Engine::updateImGui() {

    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
    ImGui::SetNextWindowSize(ImVec2(820, 600));

    ImGui::Begin("Menu");
    ImGui::Image((void*)(intptr_t)mountain_logo->getID(), ImVec2(798, 257));


    if(ImGui::CollapsingHeader("Mountains")) {
        ImGui::SliderFloat("Height Map Vertex Width", &HM_vertWidth, 0.2f, 3.0f);
        ImGui::SliderFloat("Height Map Vertex Height", &HM_vertHeight, 0.2f, 3.0f);
        ImGui::SliderFloat("Height Map Mountains Frequency", &HM_frequency, 0.1f, 2.f);
        ImGui::SliderFloat("Height Map Mountains Power", &HM_power, 0.1f, 1.f);

        if(ImGui::Button("GENERATE NEW HEIGHTMAP")){
            initHeightMap();
            initShadowMap();
            initTextures();
            initTextureBindings();
        }
    }

    if(ImGui::CollapsingHeader("Lighting")){
        ImGui::SliderFloat("Sun Position", &sun_x_orientation, -25000.f, 25000.f);

        if(ImGui::Button("GENERATE NEW SUN AND SHADOWMAP")){
            initLights();
            initShadowMap();
            initTextures();
            initTextureBindings();
            initSun();
        }
    }

    if(ImGui::CollapsingHeader("Display Height Map")){
        ImGui::Image((void*)(intptr_t)heightmap->getID(), ImVec2(400, 400));
    }
    if(ImGui::CollapsingHeader("Display Shadow Map")){
        ImGui::Image((void*)(intptr_t)shadowmap->getID(), ImVec2(400, 400));
    }


    if(ImGui::CollapsingHeader("Informations")) {
        ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
        ImGui::Text("Project made by Nathan DELAIRE and Matthieu BOLLIAND");
        ImGui::Text("July 2021 - EPITA - POGL");
    }

    ImGui::End();
}


void Engine::updateUniforms() {
    //Get new width and height from window in case of window size changing
    glfwGetFramebufferSize(window, &fbwidth, &fbheight);

    //Update matrices translations -> Things that change every frame
    ViewMatrix = camera.getViewMatrix();

    ProjectionMatrix = glm::perspective(glm::radians(camera.fov),
                                        static_cast<float>(fbwidth) / fbheight,
                                        0.1f, 100000.f);

    shader->use();
    shader->setMat4("projection", ProjectionMatrix);
    shader->setMat4("view", ViewMatrix);
    shader->setVec3("lightPosition", sunlight->position);
    shader->setVec3("lightColor", sunlight->color);
    shader->setFloat("shineDamper", 1);
    shader->setFloat("reflectivity", 0);
    shader->unuse();

    skybox->updateUniforms(ProjectionMatrix, ViewMatrix);
    sun->updateUniforms(ProjectionMatrix, ViewMatrix);

}


void Engine::update() {
    glfwPollEvents();
    updateDeltaTime();
    updateKeyboardInput();
    if(!is_Pause)
        updateMouseInput();
    updateImGui();
    updateUniforms();
    camera.updateInput(dt, -1, mouseOffsetX, mouseOffsetY);

}

//=================== RENDER ==================//

void Engine::render() {

    glViewport(0, 0, fbwidth, fbheight);
    glClearColor(0.4f, 0.4f, 0.4f, 1.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    ground->render(shader);
    sun->render();
    skybox->render();

    ImGui::Render();
    if(is_Pause)
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    glfwSwapBuffers(window);
    glFlush();
}

