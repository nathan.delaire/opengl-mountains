//
// Created by Pierre on 13/07/2021.
//

#ifndef OPENGL_MOUNTAINS_TEXTURE_HPP
#define OPENGL_MOUNTAINS_TEXTURE_HPP

#include <string>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GL/glew.h>
#include <iostream>
#include "../utils/stb_image.h"


class Texture {
public:

    Texture(std::string filename);
    Texture(std::string filename, std::vector<float> tc);

    void bind();
    void unbind();

    unsigned int getWidth() const { return w; }
    unsigned int getHeight() const { return h; }
    unsigned int getID() const { return textureID; }

    std::string getFilePath() const { return filepath; }

    GLvoid* getImageData();

    GLfloat texCoords[8];

private:

    std::string &filepath;
    unsigned int textureID;
    unsigned int w, h; //normalized
    unsigned int x, y; //coordinates normalized

    unsigned int load();
};


#endif //OPENGL_MOUNTAINS_TEXTURE_HPP
