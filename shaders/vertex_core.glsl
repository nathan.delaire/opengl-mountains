#version 440 core

vec3 getNormal();

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 texCoords;

out DATA {
    vec3 position;
    vec3 surfaceNormal;
    vec3 lightPosition;
    vec2 texCoords;
    vec4 FragPosLightSpace;
} vs_out;

uniform mat4 projection;
uniform mat4 view = mat4(1.0);
uniform mat4 model = mat4(1.0);
uniform vec3 lightPosition;
uniform mat4 lightSpaceMatrixShadow;

uniform sampler2D heightMap;

uniform float AMPLITUDE;

void main() {

    //Height of mountains
    float height = texture(heightMap, texCoords).r;
    height = height * AMPLITUDE;

    vec4 worldPosition = model * vec4(position.x, height, position.z, 1.0);

    gl_Position = projection * view * worldPosition;
    //gl_Position = lightSpaceMatrixShadow * model * vec4(position.x, height, position.z, 1.0);

    vs_out.position = vec3(position.x, height, position.z);

    // Multiply it so the textures repeat, instead of stretching
    vs_out.texCoords = texCoords * 80;

    vs_out.surfaceNormal = getNormal();
    vs_out.lightPosition = lightPosition;

    vs_out.FragPosLightSpace = lightSpaceMatrixShadow * worldPosition;
}

vec3 getNormal() {
    float offset = 1.0 / 256.0;
    float hL = texture2D(heightMap, texCoords + vec2(-offset, 0)).x;
    float hR = texture2D(heightMap, texCoords + vec2(offset, 0)).x;
    float hD = texture2D(heightMap, texCoords + vec2(0, -offset)).x;
    float hU = texture2D(heightMap, texCoords + vec2(0, offset)).x;
    vec3 n = vec3(hL - hR, 2.0f, hD - hU);

    return normalize(n);
}