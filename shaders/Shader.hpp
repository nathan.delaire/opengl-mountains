//
// Created by Pierre on 12/07/2021.
//

#ifndef OPENGL_MOUNTAINS_SHADER_HPP
#define OPENGL_MOUNTAINS_SHADER_HPP

#include "../libs.h"

class Shader {

public:

    Shader(const char* vFile,
           const char* gFile,
           const char* fFile);

    ~Shader();

    void use() const;
    void unuse() const;

    // Utility uniform functions
    void setBool(const std::string &name, bool value) const {
        glUniform1i(glGetUniformLocation(id, name.c_str()), (int)value);
    }
    void setInt(const std::string &name, int value) const {
        glUniform1i(glGetUniformLocation(id, name.c_str()), value);
    }
    void setFloat(const std::string &name, float value) const {
        glUniform1f(glGetUniformLocation(id, name.c_str()), value);
    }
    void setVec2(const std::string &name, glm::vec2 vec) const {
        glUniform2f(glGetUniformLocation(id, name.c_str()), vec.x, vec.y);
    }
    void setVec2(const std::string &name, float x, float y) const {
        glUniform2f(glGetUniformLocation(id, name.c_str()), x, y);
    }
    void setVec3(const std::string &name, glm::vec3 vec) {
        glUniform3fv(glGetUniformLocation(id, name.c_str()), 1, glm::value_ptr(vec));
    }
    void setVec3(const std::string &name, float x, float y, float z) const {
        glUniform3f(glGetUniformLocation(id, name.c_str()), x, y, z);
    }
    void setVec4(const std::string &name, glm::vec4 vec) {
        glUniform4fv(glGetUniformLocation(id, name.c_str()), 1, glm::value_ptr(vec));
    }
    void setMat3(const std::string &name, glm::mat4 mat) const {
        glUniformMatrix3fv(glGetUniformLocation(id, name.c_str()), 1, GL_FALSE, glm::value_ptr(mat));
    }
    void setMat4(const std::string &name, glm::mat4 mat) const {
        glUniformMatrix4fv(glGetUniformLocation(id, name.c_str()), 1, GL_FALSE, glm::value_ptr(mat));
    }
    void setTexture(const std::string &name, unsigned int textureLocation) {
        glUniform1i(glGetUniformLocation(id, name.c_str()), textureLocation);
    }

    GLuint id;
private:

    std::string loadShaderSource(const char* filename);

    GLuint loadShader(GLenum type, const char* filename);

    void linkProgram(GLuint vShader,
                     GLuint gShader,
                     GLuint fShader);

};


#endif //OPENGL_MOUNTAINS_SHADER_HPP
