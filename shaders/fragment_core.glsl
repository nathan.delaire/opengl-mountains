#version 420 core

//vec3 getNormal();

out vec4 fragColor;

in DATA {
    vec3 position;
    vec3 surfaceNormal;
    vec3 lightPosition;
    vec2 texCoords;
    vec4 FragPosLightSpace;
} fs_in;

//Textures for mountains
uniform sampler2D tex_grass;
uniform sampler2D tex_rock1;
uniform sampler2D tex_rock2;
uniform sampler2D tex_snow;

uniform sampler2D heightMap;
uniform sampler2D shadowMap;
uniform samplerCube skybox;

uniform vec3 lightColor;
uniform float shineDamper = 1;
uniform float reflectivity = 0.0;

void main() {

    vec4 snow = texture(tex_snow, fs_in.texCoords);
    vec4 grass = texture(tex_grass, fs_in.texCoords);
    vec4 rock1 = texture(tex_rock1, fs_in.texCoords);
    vec4 rock2 = texture(tex_rock2, fs_in.texCoords);

    // Light calculations
    vec3 lightDir = normalize(fs_in.lightPosition - fs_in.position);
    float diff = max(dot(fs_in.surfaceNormal, lightDir), 0.0);
    vec3 diffuse = diff * lightColor;

    float heightColor = texture(heightMap, fs_in.texCoords / 80).r;
    float shadowColor = texture(shadowMap, fs_in.texCoords / 80).r;

    vec4 totalColor;

    vec4 RS = mix(rock1, snow, heightColor * 1.2);
    vec4 GR = mix(grass, rock2, heightColor * 1.4);
    totalColor = mix(GR, RS, heightColor * 1.2);

    fragColor = vec4(totalColor.x, totalColor.y, totalColor.z, 1.f) * vec4(diffuse, 1.f) * vec4(vec3(shadowColor), 1.f);
}