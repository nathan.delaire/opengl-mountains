//
// Created by Pierre on 12/07/2021.
//

#include "../utils/noiseutils.hpp"

#ifndef OPENGL_MOUNTAINS_HEIGHTMAP_HPP
#define OPENGL_MOUNTAINS_HEIGHTMAP_HPP

utils::Image createHeightMap(float noiseWidth, float noiseHeight, float vertWidth, float vertHeight,
                             float frequency, float power);

#endif //OPENGL_MOUNTAINS_HEIGHTMAP_HPP
