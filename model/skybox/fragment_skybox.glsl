#version 440

in vec3 textureCoords;
out vec4 fragColor;

uniform samplerCube cubeMap;

void main(){
    fragColor = texture(cubeMap, textureCoords);
    //fragColor = vec4(1.f, 0.f, 0.f, 1.f);
}