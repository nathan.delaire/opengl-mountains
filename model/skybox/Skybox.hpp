//
// Created by Pierre on 14/07/2021.
//

#ifndef OPENGL_MOUNTAINS_SKYBOX_HPP
#define OPENGL_MOUNTAINS_SKYBOX_HPP

#include "../../shaders/Shader.hpp"
#include "../Model.hpp"
#include "../../utils/stb_image.h"
#include <vector>
#include <string>

class Skybox {
public:
    Skybox(glm::mat4 projection, glm::mat4 view);
    void render();

    void updateUniforms(glm::mat4 projection, glm::mat4 view);

    Shader *shader;

private:
    void initSkyBoxtexture();
    void initSkyBoxModel();
    void initShader();
    void initTextureBindings();
    void initUniforms();


    float SIZE = 50000.f;
    Model *box;

    glm::mat4 ProjectionMatrix;
    glm::mat4 ViewMatrix;

    unsigned int skybox_texture_id;
};



#endif //OPENGL_MOUNTAINS_SKYBOX_HPP
