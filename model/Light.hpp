//
// Created by Pierre on 13/07/2021.
//

#ifndef OPENGL_MOUNTAINS_LIGHT_HPP
#define OPENGL_MOUNTAINS_LIGHT_HPP

#include <glm/vec3.hpp>

struct Light{
    glm::vec3 position;
    glm::vec3 direction;
    glm::vec3 color;
};

#endif //OPENGL_MOUNTAINS_LIGHT_HPP
