![](./img/logo.png)

![](./img/mountains.PNG)

# Auteurs

Ce projet a été réalisé par Nathan DELAIRE et Matthieu BOLLIAND dans le cadre de la matière Programmation OpenGL au sein de l'EPITA.

# Présentation globale
![](./img/mountains.png)
Ce projet consiste à générer des chaînes de montagnes en créant au préalable une Heightmap loadé en texture au sein des shaders. Il aborde différente notions vue ou non en cours :
* Multi-texturing
* Cubemap / Skybox
* Multiple shaders
* Keyboard / Mouse input updates
* ImGui Interface

## Tutoriel / Démo

Je vous conseille <span style="color:red"> **TRES FORTEMENT** </span> de regarder la vidéo suivante afin de comprendre le projet dans sa globalité, comment se déplacer dans le monde et interagir avec les différentes options de personnalisation proposées dans ce projet :

https://www.youtube.com/watch?v=QwoS0DgyRSQ

Tout est expliqué dans la vidéo tutoriel, mais voici un rapide résumé des différents contrôles :
* **ZQSD** : Avancer / Gauche / Reculer / Droite
* **Espace** : Monter
* **C** : Descendre
* **F1** : Ouvrir menu de personnalisation
* **F2** : Fermer menu de personnalisation

## Pré-requis
Pour faire compiler ce projet, vous allez avior besoin des libs suivantes :
* CMake
* OpenGL (version minimum 4.4)
* GLFW 3
* GLM
* Libnoise (pour la génération des heightmaps)

## Installation
<span style="color:yellow">  Il est fortement conseillé de faire tourner l'application sur Windows, le projet n'a pas été testé sous Linux.</span>

Dans l'archive fournie avec le projet, vous aurez un **Mountains.exe** dans le dossier cmake-build-debug du projet. 
Mais dans le cas ou vous n'auriez pas l'application, le projet se compile comme une application cmake classique :

``
cmake  -DCMAKE_BUILD_TYPE=Release -G "CodeBlocks - MinGW Makefiles" .
``

``
cmake --build . --target Raymarching -- -j 12
``

Pour **Linux**, les lignes sont les mêmes en enlevant bien-sûr l'option ``G`` de la première ligne.

<span style="color:red"> IMPORTANT : Peu importe  comment vous compilez le projet, il est très important de placer l'application .exe dans un sous dossier du projet (exemple, un dossier build à l'intérieur de opengl-mountains), sans quoi le projet ne va pas tourner pour cause d'erreur au seins des différent paths du code. </span> 



## Liens externe :
* Git du projet : https://gitlab.com/nathan.delaire/opengl-mountains.git


